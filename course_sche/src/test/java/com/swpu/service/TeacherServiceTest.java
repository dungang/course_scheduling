package com.swpu.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-service.xml",
        "classpath:application-dao.xml"})
public class TeacherServiceTest {
    @Autowired
    private TeacherService teacherService;
    @Test
    public void findTeacherById() {
        System.out.println(teacherService.findTeacherById("202031772130"));
    }
}
