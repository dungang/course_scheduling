package com.swpu.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-service.xml",
        "classpath:application-dao.xml"})
public class TeacherMapperTest {
    @Autowired
    private TeacherMapper teacherMapper;

    @Test
    public void findTeacherByIdTest() {
        System.out.println(teacherMapper.findTeacherById("202031772130"));
    }

    @Test
    public void findAllTeachersTest() {
        System.out.println(teacherMapper.findAllTeachers());
    }

    @Test
    public void findTeachersByNameTest() {
        System.out.println(teacherMapper.findTeachersByName("陈"));
    }

    @Test
    public void addTeacherTest() {
        int n = teacherMapper.addTeacher("202031772136", "dj",
                "1111111111", "男", "19536547892");
        if(n == 1) {
            System.out.println(teacherMapper.findAllTeachers());
        }
    }

    @Test
    public void updatePasswordByIdTest() {
        int n = teacherMapper.updatePasswordById("123456789", "202031772136");
        if(n == 1) {
            System.out.println(teacherMapper.findAllTeachers());
        }
    }

    @Test
    public void deleteTeacherByIdTest() {
        int n = teacherMapper.deleteTeacherById("202031772136");
        if(n == 1) {
            System.out.println(teacherMapper.findAllTeachers());
        }
    }
}
