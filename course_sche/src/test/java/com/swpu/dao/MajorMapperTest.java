package com.swpu.dao;

import com.swpu.pojo.Major;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-dao.xml"})
public class MajorMapperTest {
    @Autowired
    private MajorMapper majorMapper;
    @Test
    public void findAllMajor(){
        List<Major> majors=majorMapper.findAllMajor();
        for(Major major:majors){
            System.out.println(major);
        }
    }
    @Test
    public void findMajorById(){
        Major major=majorMapper.findMajorById("30781301");
        System.out.println(major);
    }
    @Test
    public void findMajorsByName(){
        List<Major> majors=majorMapper.findMajorsByName("网络");
        for (Major major:majors){
            System.out.println(major);
        }
    }
    @Test
    public void addMajor(){
        int a=majorMapper.addMajor("31771313","信息管理与信息系统","2022",120);
        if(a == 1) {
            System.out.println(majorMapper.findAllMajor());
        }
    }
    @Test
    public void deleteMajorById(){
        int a=majorMapper.deleteMajorById("31771313");
        if (a==1)
            System.out.println(majorMapper.findAllMajor());
    }
}