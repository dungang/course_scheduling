package com.swpu.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-service.xml",
        "classpath:application-dao.xml"})
public class CourseMapperTest {
    @Autowired
    private CourseMapper courseMapper;

    @Test
    public void findCourseByIdTest() {
        System.out.println(courseMapper.findCourseById("5401067030"));
    }

    @Test
    public void findAllCoursesTest() {
        System.out.println(courseMapper.findAllCourses());
    }

    @Test
    public void findCoursesByNameTest() {
        System.out.println(courseMapper.findCoursesByName("企业"));
    }

    @Test
    public void addCourseTest() {
        int n = courseMapper.addCourse("7301012056", "hello world",
                "802", 32, 16,"202031772134");
        if(n == 1) {
            System.out.println(courseMapper.findAllCourses());
        }
    }

    @Test
    public void deleteCourseByIdTest() {
        int n = courseMapper.deleteCourseById("7301012056");
        if(n == 1) {
            System.out.println(courseMapper.findAllCourses());
        }
    }
}
