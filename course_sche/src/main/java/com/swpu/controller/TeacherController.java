package com.swpu.controller;

import com.swpu.pojo.Teacher;
import com.swpu.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TeacherController {
    @Autowired
    private  TeacherService teacherService;

    @RequestMapping("/teacher")
    public ModelAndView findTeacherById(String id){
        Teacher teacher=teacherService.findTeacherById(id);
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.setViewName("teacher.jsp");
        modelAndView.addObject("teacher",teacher);
        return modelAndView;
    }
}
