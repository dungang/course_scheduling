package com.swpu.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Course {
    private String id;
    private String name;
    private String course_number;
    private Integer total_hours;
    private Integer practice_hours;
    private String teacher_id;
}
