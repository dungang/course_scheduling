package com.swpu.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseScheduling {
    private String id;
    private String course_id;
    private String major_id;
    private String room_id;
    private String week_during;
    private String week_day;
    private String section;
}
