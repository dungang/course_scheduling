package com.swpu.dao;

import com.swpu.pojo.Major;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MajorMapper {
    List<Major> findAllMajor();
    Major findMajorById(String id);
    List<Major> findMajorsByName(String name);
    Integer addMajor(String id,String name,String grade,Integer number);
    Integer deleteMajorById(String id);
}