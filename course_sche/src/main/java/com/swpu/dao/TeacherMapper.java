package com.swpu.dao;

import com.swpu.pojo.Teacher;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherMapper {
    Teacher findTeacherById(String id);
    List<Teacher> findAllTeachers();
    List<Teacher> findTeachersByName(String name);
    int updatePasswordById(String password, String id);
    int addTeacher(String id, String name, String password, String sex, String phone, String role);
    int deleteTeacherById(String id);
}
