package com.swpu.dao;

import com.swpu.pojo.Course;
import com.swpu.pojo.Teacher;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseMapper {
    Course findCourseById(String id);
    List<Course> findAllCourses();
    List<Course> findCoursesByName(String name);
    int addCourse(String id, String name, String course_number, int total_hours, int practice_hours, String teacher_id);
    int deleteCourseById(String id);
}
