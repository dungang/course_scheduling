package com.swpu.dao;

import com.swpu.pojo.Room;
import com.swpu.pojo.Teacher;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomMapper {
    Room findRoomById(String id);
    List<Room> findAllRooms();
    List<Room> findRoomsByName(String name);
    int addRoom(String id, String name, String capacity);
    int deleteRoomById(String id);
}
