package com.swpu.service;

import com.swpu.pojo.Teacher;

public interface TeacherService {
    public Teacher findTeacherById(String id);
}
