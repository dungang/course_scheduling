package com.swpu.service.Impl;

import com.swpu.dao.TeacherMapper;
import com.swpu.pojo.Teacher;
import com.swpu.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherMapper teacherMapper;
    @Override
    public Teacher findTeacherById(String id) {
        return teacherMapper.findTeacherById(id);
    }
}
