drop database if exists ccdb;
create database ccdb;

use ccdb;
drop table if exists teacher;
create table teacher(
    id varchar(12) primary key,
    `name` varchar(20) not null, # 姓名
    `password` varchar(12) not null,
    sex varchar(10),
    phone varchar(11) not null,
    `role` int not null
);
use ccdb;
insert into teacher(id,`name`,`password`,sex,phone,`role`) values
('202031772130','张杰','123456','男','19161056817','1'),
('202031772131','谢楠','789456','女','19161056890','0'),
('202031772132','陈志','222222','男','19161056802','0'),
('202031772133','陈星系','123444','男','19161054563','0'),
('202031772134','张宇','111111','男','19161058888','1');

use ccdb;
drop table if exists room; # 机房
create table room(
    id varchar(12) primary key,
    `name` varchar(20) not null, # 机房名
    capacity int not null # 机房容量
);
use ccdb;
insert into room values('802','第一实验楼八楼802','60');
insert into room values('804','第一实验楼八楼804','60');
insert into room values('812','第一实验楼八楼812','60');
insert into room values('4205','四教二楼4205','60');
insert into room values('4206','四教二楼4206','60');
insert into room values('4302','四教三楼4302','120');
insert into room values('4414','四教四楼4414','120');
insert into room values('4512','四教五楼4512','120');

use ccdb;
drop table if exists major;  # 专业
create table major(
    id varchar(12) primary key, # 专业代码
    `name` varchar(32) not null, # 专业名称
    grade varchar(32) not null, # 专业年级
    `number` int not null # 专业人数
);
use ccdb;
insert into major values('31771301','信息管理与信息系统','2020','120');
insert into major values('31771312','信息管理与信息系统','2021','121');
insert into major values('31441301','电子与计算机技术','2020','130');
insert into major values('30781301','网络工程','2020','125');
insert into major values('31441322','电子与计算机技术','2022','125');
insert into major values('30781311','网络工程','2021','110');

use ccdb;
drop table if exists course;
create table course(
    id varchar(12) primary key,
    `name` varchar(32) not null,
    course_number  varchar(12) not null, # 课序号or教学班
    total_hours int not null, # 总学时
    practice_hours int not null,
    teacher_id varchar(12) not null,
    foreign key(teacher_id) references teacher(id)
);
use ccdb;
insert into course values('5401067030','企业级应用开发','812','32','16','202031772130');
insert into course values('7019009030','数据可视化技术','4205','32','16','202031772130');
insert into course values('7019015030','程序设计模式','804','32','16','202031772132');
insert into course values('7301012030','软件质量保证与测试','802','32','16','202031772133');
insert into course values('2001006002','形势与政策6','4414','8','0','202031772134');

use ccdb;
drop table if exists course_scheduling;
create table course_scheduling(
    id varchar(12) primary key,
    course_id varchar(12) not null,
    major_id varchar(12) not null,
    room_id varchar(12) not null,
    # 每个专业同样的课程安排不同周
    week_during varchar(12),
    week_day varchar(12),
    section varchar(12), # 上午5节，下午4节
    foreign key(course_id) references course(id),
    foreign key(major_id) references major(id),
    foreign key(room_id) references room(id)
);
use ccdb;
insert into course_scheduling values('1','5401067030','31771301','802','1,8','1,3,5','1,2');
insert into course_scheduling values('2','7019009030','31771312','804','1,12','2,4','3,4');
insert into course_scheduling values('3','7301012030','31441301','4512','1,8','1,5','6,7');
insert into course_scheduling values('4','2001006002','30781311','4414','9,12','1,4','8,9');

use ccdb;
select * from teacher;
select * from course;
select * from major;
select * from room;
select * from course_scheduling;