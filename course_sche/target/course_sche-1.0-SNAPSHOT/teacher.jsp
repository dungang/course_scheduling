<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head><title>教师信息查询</title></head>
<body>
<table border="1">
    <tr>
        <th>教师id</th>
        <th>教师名称</th>
        <th>教师性别</th>
        <th>教师电话</th>
    </tr>
    <tr>
        <td>${teacher.tea_id}</td>
        <td>${teacher.tea_name}</td>
        <td>${teacher.sex}</td>
        <td>${teacher.phone}</td>
    </tr>
</table>
</body>
</html>

